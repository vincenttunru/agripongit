import React, { FC } from "react";
import { sha256 } from "hash.js";
import { Command } from "../tutorial";
import { animated, useSpring, useTransition } from "react-spring";

interface Props {
  commands: Array<Command>;
}

export const Repository: FC<Props> = (props) => {
  const repoState = props.commands.reduce(applyCommand, undefined);
  const empyRepoStyles = useSpring({
    opacity: repoState === null ? 1 : 0,
    config: {
      friction: 40,
    },
  })

  if (repoState === undefined) {
    return null;
  }

  if (repoState === null) {
    return (
      <div className="flex items-center justify-center h-screen">
        <animated.div style={empyRepoStyles} className="border-dashed border-4 border-gray-200 text-gray-200 font-bold w-1/2 px-10 py-28 text-center">
          Empty repository
        </animated.div>
      </div>
    );
  }

  const layout = calculateLayout(repoState.local.root, repoState.local.commits);

  const edges = drawEdges(layout, layout, repoState.local.commits);
  const nodes = drawNodes(layout);
  const head = drawHead(repoState.local.head, layout);
  const branches = drawBranches(repoState.local.currentBranch, repoState.local.branches, layout);

  const elements = edges.concat(nodes);

  return (
    <svg
      version="1.1"
      width="100%"
      height="100%"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid slice"
      id="repository"
      className="block"
    >
      {head}
      {branches}
      <Elms elements={elements}/>
    </svg>
  );
};

const Elms: FC<{ elements: JSX.Element[] }> = (props) => {
  const transitions = useTransition(
    props.elements,
    element => element.key!,
    {
      from: { opacity: 0 },
      enter: { opacity: 1 },
      leave: { opacity: 0 },
      config: {
        friction: 40,
      },
    },
  );

  return <>
    {
      transitions.map(({item, props, key}) => {
        return <animated.g key={key} style={props}>{item}</animated.g>;
      })
    }
  </>;
}

type CommitHash = string;
type BranchName = string;
type RemoteName = string;
interface Commit {
  parents: CommitHash[];
}
type Remote = {
  commits: Record<CommitHash, Commit>;
  root: CommitHash;
  head: CommitHash;
  currentBranch: BranchName;
  branches: Record<BranchName, CommitHash>;
};
interface RepoState {
  local: Remote;
  remotes: Record<RemoteName, Remote>;
}

// function applyCommand(repoState: null, command: "init"): RepoState;
// function applyCommand(repoState: RepoState, command: Omit<Command, "init">): RepoState;
function applyCommand(repoState: RepoState | null | undefined, command: Command, i: number): RepoState | null {
  const commandName = command.command;
  if (repoState === undefined) {
    if (commandName !== "init") {
      throw new Error("No Git repository initialised.");
    }
    return null;
  }
  if (repoState === null) {
    if (commandName === "commit") {
      const rootCommit: Commit = {
        parents: [],
      };
      const rootCommitHash = generateCommitHash(i);
      return {
        local: {
          commits: {
            [rootCommitHash]: rootCommit,
          },
          root: rootCommitHash,
          head: rootCommitHash,
          currentBranch: "main",
          branches: { main: rootCommitHash }
        },
        remotes: {},
      };
    }
    throw new Error("Invalid command");
  }
  /* eslint-disable no-case-declarations */
  switch(commandName) {
    case "commit":
      const newCommitHash = generateCommitHash(i);
      const newCommit: Commit = {
        parents: [repoState.local.head],
      };
      const commits = {
        ...repoState.local.commits,
        [newCommitHash]: newCommit,
      };
      return {
        ...repoState,
        local: {
          ...repoState.local,
          commits: commits,
          head: newCommitHash,
          branches: {
            ...repoState.local.branches,
            [repoState.local.currentBranch]: newCommitHash,
          },
        },
      };
    case "branch":
      const newBranchName = command.parameter ?? "myBranch";
      return {
        ...repoState,
        local: {
          ...repoState.local,
          branches: {
            ...repoState.local.branches,
            [newBranchName]: repoState.local.head,
          },
        },
      };
    case "switch":
      const targetBranch = command.parameter ?? Object.keys(repoState.local.branches).find(otherBranch => otherBranch !== repoState.local.currentBranch);
      if (typeof targetBranch !== "string") {
        throw new Error("No other branch to switch to.");
      }
      return {
        ...repoState,
        local: {
          ...repoState.local,
          currentBranch: targetBranch,
          head: repoState.local.branches[targetBranch],
        },
      };
    case "merge":
      const otherBranch = command.parameter ?? Object.keys(repoState.local.branches).find(otherBranch => otherBranch !== repoState.local.currentBranch);
      if (typeof otherBranch !== "string") {
        throw new Error("No branch to merge from.");
      }
      const mergeCommitHash = generateCommitHash(i);
      const mergeCommit: Commit = {
        parents: [
          repoState.local.branches[otherBranch],
          repoState.local.branches[repoState.local.currentBranch],
        ],
      };
      return {
        ...repoState,
        local: {
          ...repoState.local,
          commits: {
            ...repoState.local.commits,
            [mergeCommitHash]: mergeCommit,
          },
          branches: {
            ...repoState.local.branches,
            [repoState.local.currentBranch]: mergeCommitHash,
          },
          head: mergeCommitHash,
        },
      };
    case "remote_add":
      // We don't actually do anything here; it's only when we push that things change.
      return repoState;
    case "fetch":
      const remoteToFetch = command.parameter ?? "origin";
      // Pretend that a commit to the remote branch has been fetched:
      return applyCommand(
        applyCommand(
          applyCommand(repoState, { command: "switch", parameter: `${remoteToFetch}/main` }, i + 0.1),
          { command: "commit" },
          i + 0.2,
        ),
        { command: "switch", parameter: repoState.local.currentBranch },
        i + 0.3,
      );
    case "push":
      const remoteToPushTo = command.parameter ?? "origin";
      return applyCommand(repoState, { command: "branch", parameter: `${remoteToPushTo}/main` }, i + 0.1);
    default:
      return new Error("Unkown command") as any as never;
  }
}
/* eslint-enable no-case-declarations */

function generateCommitHash(seed: number): CommitHash {
  return sha256().update(seed.toString()).digest("hex");
}


type NodeLayout = {
  x: number;
  y: number;
  children: NodeLayout[];
  labelPosition: "left" | "right";
  commitHash: CommitHash;
};
type Range = {
  from: number;
  to: number;
};
const radius = 2.5;
// This margin gives us room to place the labels.
const margin = 30;
const fontSize = radius * 3/4;
function calculateLayout(
  current: CommitHash,
  commits: Record<CommitHash, Commit>,
  y: number = 4 * radius,
  range: Range = { from: margin, to: 100 - margin },
  labelPosition: NodeLayout['labelPosition'] = "right",
): NodeLayout {
  const x = range.from + ((range.to - range.from) / 2);

  const children = Object.keys(commits).filter(commitHash => commits[commitHash].parents[0] === current);
  const childRangeWidth = (range.to - range.from)/(children.length);

  const childrenLayout = children.map((childCommitHash, i) => {
    const childRange: Range = children.length === 1
      ? range
      : {
        from: range.from + childRangeWidth * i,
        to: range.from + childRangeWidth * (i+1),
      };
    const labelPosition = (children.length > 1 && i < children.length / 2) ? "left" : "right"
    return calculateLayout(
      childCommitHash,
      commits,
      y + radius * 3.5,
      childRange,
      labelPosition,
    );
  });

  return {
    children: childrenLayout,
    x: x,
    y: y,
    labelPosition: labelPosition,
    commitHash: current,
  };
}

function drawNodes(layout: NodeLayout): JSX.Element[] {
  const elements = [
    <circle cx={layout.x} cy={layout.y} r={radius}
      className="fill-current text-blue-500"
      key={`commit-node-${layout.commitHash}`}
    />,
  ];
  return elements.concat(layout.children.map((child) => drawNodes(child)).flat());
}

function drawEdges(layout: NodeLayout, layoutRoot: NodeLayout, commits: Record<CommitHash, Commit>): JSX.Element[] {
  return layout.children.map(child => {
    const childCommit = commits[child.commitHash];
    const parentLayouts = childCommit.parents.map(parentCommit => getPositionForCommit(parentCommit, layoutRoot)).filter(isNotNull);
    const elements = parentLayouts.map(parentLayout => (
      <line
        strokeWidth={ radius / 20 }
        className="stroke-current text-blue-100 stroke-1"
        key={`line-${parentLayout.commitHash}-${child.commitHash}`}
        x1={parentLayout.x} y1={parentLayout.y}
        x2={child.x} y2={child.y}
      />
    ));
    return elements.concat(drawEdges(child, layoutRoot, commits));
  }).flat();
}

function drawHead(head: CommitHash, layout: NodeLayout): JSX.Element | null {
  const relevantPosition = getPositionForCommit(head, layout);

  if (relevantPosition === null) {
    return null;
  }

  const xPosition = relevantPosition.labelPosition === "left"
    ? relevantPosition.x - (radius + 1)
    : relevantPosition.x + (radius + 1);
  const textAnchor = relevantPosition.labelPosition === "left"
    ? "end"
    : "start";

  return <>
    <text
      key="head"
      id="head"
      x={xPosition}
      y={relevantPosition.y - radius}
      textAnchor={textAnchor}
      fontSize={fontSize}
      className="uppercase fill-current text-green-300 font-bold font-mono"
    >Head</text>
  </>;
}

function drawBranches(currentBranch: BranchName, branches: Record<BranchName, CommitHash>, layout: NodeLayout): JSX.Element[] {
  return Object.keys(branches).map(branchName => {
    const relevantPosition = getPositionForCommit(branches[branchName], layout);
    if (relevantPosition === null) {
      throw new Error(`Could not find the relevant node for branch [${branchName}].`)
    }

    const xPosition = relevantPosition.labelPosition === "left"
      ? relevantPosition.x - (radius + 1)
      : relevantPosition.x + (radius + 1);
    const textAnchor = relevantPosition.labelPosition === "left"
      ? "end"
      : "start";

    const branchesForThisCommit = Object.keys(branches).filter(otherBranch => branches[otherBranch] === branches[branchName]);
    const thisBranchNr = branchesForThisCommit.indexOf(branchName);

    const currentBranchClasses = branchName === currentBranch
      ? "text-yellow-200 font-bold"
      : "text-white";

    return (
      <>
        <text
          className={`fill-current font-mono ${currentBranchClasses}`}
          key={`branch-${branchName}`}
          x={xPosition}
          textAnchor={textAnchor}
          // Y position of associated node
          // - radius (to top-align with the node)
          // + height of a branch name * (number of branches listed above it + height of the HEAD label)
          y={relevantPosition.y - radius + fontSize * (thisBranchNr +  1)}
          fontSize={fontSize}
        >
          {branchName}
        </text>
      </>
    );
  });
}

function getPositionForCommit(commitHash: CommitHash, layout: NodeLayout): NodeLayout | null {
  if (layout.commitHash === commitHash) {
    return layout;
  }
  return layout.children.map(child => getPositionForCommit(commitHash, child)).find(isNotNull) ?? null;
}

function isNotNull<T>(value: T | null): value is T {
  return value !== null;
}
