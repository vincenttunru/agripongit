import React from "react";
import { Code, Command } from "./components/html";

export type Command = {
  command: "init" | "commit" | "branch" | "switch" | "merge" | "remote_add" | "push" | "fetch";
  parameter?: string;
};

export type Section = {
  text: JSX.Element;
  command: Command;
};

export type Tutorial = Array<Section>;

export const tutorial: Tutorial = [
  {
    command: { command: "init" },
    text: (
      <>
        <p>Imagine we're starting a new project that we want to manage using Git. This could be any type of project, but for the sake of this tutorial, let's say we're writing a book. The first order of action is to make Git aware of it. Inside the folder that is to house our book we initialise a new Git repository:</p>
        <Command>git init</Command>
        <p>Git has created a new hidden folder called <Code>.git</Code> — everything that Git knows about our book will live in there, <em>separate</em> from our actual project.</p>
        <p>Now let's say that we've already created two files: <Code>thanks.txt</Code>, in which we will keep track of who we need to include in the word of thanks, and <Code>outline.txt</Code>, which contains a general outline of what our book will be about.</p>
        <p>Let's give Git an actual version to control — it is, after all, a <i>version control system</i>. You have to make a conscious choice about what each version (or in Git&nbsp;speak: <em>commit</em>) includes; for now, we only <Code>git add thanks.txt</Code>. This is called <em>staging</em> the file <Code>thanks.txt</Code>: marking it for inclusion included in the next commit.</p>
      </>
    ),
  },
  {
    command: { command: "commit" },
    text: (
      <>
        <p>This is what our Git history looks like after our first</p>
        <Command>git commit</Command>
        <p>The circle represents this first commit — please disregard the labels next to it for now.</p>
        <p>If, later on in our project, we revert back to this commit, all we would be left with is <Code>thanks.txt</Code> in its current state.</p>
        <p>So why did we not include <Code>outline.txt</Code> in our first commit? Basically, a good rule-of-thumb is that a commit should only contain changes that can be undone together. For example, if we later want to overhaul the structure of our book, we might want to ditch the outline we just wrote without also throwing away our list of people to thank.</p>
        <p>Therefore, let us tell Git about <Code>outline.txt</Code> in a separate commit.</p>
      </>
    ),
  },
  {
    command: { command: "commit" },
    text: (
      <>
        <p>We stage it using <Code>git add outline.txt</Code>, then create another commit:</p>
        <Command>git commit</Command>
        <p>As you can see, our new commit is connected to the previous one (its <em>parent</em>). You can think of a commit as containing a list of the changes needed to turn the files as they were at the previous commit into the way they are now.</p>
        <p>Now consider the label reading <Code>HEAD</Code> next to our newly created commit. <Code>HEAD</Code> can be seen as a pointer: when a new commit is made, it will be a child of the commit <Code>HEAD</Code> is pointing to.</p>
        <p>As an example, let's say we spelled someone's name wrong. After correcting it, we <Code>git add thanks.txt</Code> again, and create a new commit:</p>
      </>
    ),
  },
  {
    command: { command: "commit" },
    text: (
      <>
        <Command>git commit</Command>
        <p>The new commit is based on the commit previously labeled <Code>HEAD</Code>, and the label is then updated to refer to the new commit as <Code>HEAD</Code>.</p>
        <p>Now let's turn to the label dubbed <Code>main</Code>. This is called a <em>branch</em>. Like <Code>HEAD</Code>, branches point to a commit. Unlike <Code>HEAD</Code>, you can name them yourself. Furthermore, you can have multiple branches.</p>
      </>
    ),
  },
  {
    command: { command: "branch" },
    text: (
      <>
        <p>We can easily create a new branch. Let's call it <Code>myBranch</Code>:</p>
        <Command>git branch myBranch</Command>
        <p>As you can see, there now are two labels pointing to the <Code>HEAD</Code> commit: <Code>main</Code> and <Code>myBranch</Code>.</p>
      </>
    ),
  },
  {
    command: { command: "commit" },
    text: (
      <>
        <p>The <em>current</em> branch is still <Code>main</Code> though. You can see what this means when we make some more changes and create another commit:</p>
        <Command>git commit</Command>
        <p>Whereas the <Code>main</Code> branch moved along with <Code>HEAD</Code>, <Code>myBranch</Code> is still pointing to the previous commit. We will see why this is useful in a bit.</p>
      </>
    ),
  },
  {
    command: { command: "switch", parameter: "myBranch" },
    text: (
      <>
        <p>We can switch to <Code>myBranch</Code>, meaning roughly that we will make <Code>HEAD</Code> point to the same commit as <Code>myBranch</Code> does:</p>
        <Command>git switch myBranch</Command>
      </>
    ),
  },
  {
    command: { command: "commit" },
    text: (
      <>
        <p>As said, a new commit will be the child of the commit <Code>HEAD</Code> is currently pointing to.</p>
        <Command>git commit</Command>
        <p>You can now see why they were called <em>branches</em>: with <Code>myBranch</Code> pointing to a different commit than <Code>main</Code>, our simple timeline has suddenly evolved into a tree structure.</p>
      </>
    ),
  },
  {
    command: { command: "commit" },
    text: (
      <>
        <p>Since we are still on <Code>myBranch</Code>, new commits will move it along with <Code>HEAD</Code>:</p>
        <Command>git commit</Command>
        <p>Branches are useful because they allow us to work on multiple things in parallel, without those things interfering with each other. For example, you could start work on a new chapter in a branch that only contains changes to that chapter. Now, when your editor is bugging you to <q>finally submit a manuscript already darnit!</q> (hypothetically), you can simply switch back to the <Code>main</Code> branch, perhaps fix up a few typos, and then send it to your editor. All this without having to include a half-finished chapter that wasn't that interesting anyway.</p>
        <p>Anyway, let's say we <em>did</em> finish that chapter. That's nice and all, but now it would be nice if we wouldn't have to redo the small changes we performed earlier in the <Code>main</Code> branch.</p>
      </>
    ),
  },
  {
    command: { command: "merge", parameter: "main" },
    text: (
      <>
        <p>Of course Git can help us here: we can <em>merge</em> the state of the files at <Code>main</Code>'s commit with the state of the files in the current branch's latest commit:</p>
        <Command>git merge main</Command>
        <p>As you can see, Git has created a new <em>merge commit</em> for us in <Code>myBranch</Code>. As opposed to regular commits, merge commits have multiple parents. This means that it contains multiple sets of changes: for each parent, the changes needed to make to it to make it incorporate the changes in the other parent(s).</p>
        <p>Now is a good time to check whether both sets of changes work well together. In the case of our book, we could check whether our edits to our earlier chapters didn't mess up the layout of our new one.</p>
        <p>If everything looks as expected, <Code>myBranch</Code> now contains the latest complete version of our book. But now imagine that we'd have had another branch with another half-finished chapter. If we were to merge <Code>myBranch</Code> into this other branch when that chapter is finished, and into another one for another chapter, and so on, then after a while we would lose track of which branch contains the latest version. It is therefore common practice to assign one branch as containing the latest complete version of a project. This branch is commonly called <Code>main</Code>, so let's stick to that here.</p>
      </>
    ),
  },
  {
    command: { command: "switch", parameter: "main" },
    text: (
      <>
        <p>However, if we switch to that branch now:</p>
        <Command>git switch main</Command>
        <p>…the chapter we wrote in <Code>myBranch</Code> is gone. The reason for this is, of course, that while we have merged <em><Code>main</Code></em> to <em><Code>myBranch</Code></em>, we haven't merged <em><Code>myBranch</Code></em> to <em><Code>main</Code></em> yet. This might feel cumbersome, but by first copying everything into <Code>myBranch</Code>, we had the opportunity to check whether everything still looked fine and dandy after the merge. Since we now know that it did, we can merge it back to <Code>main</Code> without risking it resulting in a book with a messed-up layout.</p>
      </>
    ),
  },
  {
    command: { command: "merge", parameter: "myBranch" },
    text: (
      <>
        <Command>git merge myBranch</Command>
        <p>Once again, our merge created another commit. <Code>main</Code> is now <em>ahead</em> of <Code>myBranch</Code>, since it includes commits that are not in <Code>myBranch</Code> (i.e. the merge commit).</p>
      </>
    ),
  },
  {
    command: { command: "remote_add" },
    text: (
      <>
        <p>The features we've touched upon up to now are already incredibly powerful in allowing you to work on multiple versions of the same project. They enable you to confidently make sweeping changes to your project without fear of losing anything. After all, if you're unsatisfied with your changes, you can always start anew from a commit that does not include those changes.</p>
        <p>However, Git has more tricks up its sleeve. A lot more, in fact, but for now we will only focus on how it enables effective collaboration.</p>
        <p>As we have seen, you have your own repository, consisting of the project files and a tree representing their history. The same holds true of other team members: each has a separate repository, including the project files in a certain state. None of these are special, however, a team usually decides <em>by convention</em> to have a single central repository that contains the code that will be distributed to the user later on. Much like the <Code>main</Code> branch should have the latest version of those parts of the project you've completed, the central repository should include the latest completed work by every team member. Let's look at how we can get our completed work into the central repository.</p>
        <p>Let's assume that our editor has initialised an empty Git repository at <Code>https://publisher.com/book.git</Code>. Our first step is that we need to make our local repository aware of this other repository:</p>
        <Command>git remote add publisher https://publisher.com/book.git</Command>
        <p>From our local repository's point of view, the other repositories are called <em>remotes</em>. Each remote has a name; we called the one we just added <Code>publisher</Code>.</p>
      </>
    ),
  },
  {
    command: { command: "push", parameter: "publisher" },
    text: (
      <>
        <p>In this example, there's no code in the remote yet. Let's change that by <em>pushing</em> our main branch to <Code>publisher</Code>.</p>
        <Command>git push publisher main</Command>
        <p>Several things have now happened. First, all the commits needed to go from an empty repository to the state of our local repository at <Code>main</Code> have been sent to the <Code>publisher</Code> remote. Secondly, we have a local reference to the currently known state of the remote's <Code>main</Code> branch as <Code>publisher/main</Code>. Finally, our local <Code>main</Code> branch is <em>matched</em> to <Code>publisher/main</Code>, roughly meaning that Git knows they are related.</p>
      </>
    ),
  },
  {
    command: { command: "commit" },
    text: (
      <>
        <p>We can simply continue working on our book and create a new commit as usual:</p>
        <Command>git commit</Command>
        <p>As long as do not push, our local changes will not be shared with others. While we were making our local changes, however, it is very well possible that our copy editor has proofread our first draft and submitted some corrections to the <Code>publisher</Code> repository.</p>
      </>
    ),
  },
  {
    command: { command: "fetch", parameter: "publisher" },
    text: (
      <>
        <p>To check whether this is the case, let's ask what has happened since the last time we contacted it (i.e. during our push):</p>
        <Command>git fetch publisher</Command>
        <p>And indeed, a new commit was added to <Code>publisher</Code>'s <Code>main</Code> branch. As you can see, while we weren't paying attention, our version of the branch sneakily diverged from the one at our publisher! This means we would not have been able to push the new version of our branch to our remote. It can be solved by merging the remote's work back into our own, making the history of our remote branch part of our local branch's history.</p>
      </>
    ),
  },
  {
    command: { command: "merge", parameter: "publisher/main" },
    text: (
      <>
        <p>Like with regular branches, we can merge the remote branch into our current local branch:</p>
        <Command>git merge publisher/main</Command>
        <p>Once again, we can see that a merge commit has been created.</p>
        <p>(Note that the above process of a fetch followed by a merge is fairly common. So common, in fact, that there is a single command <Code>git pull</Code> that immediately executes both.)</p>
      </>
    ),
  },
  {
    command: { command: "push", parameter: "publisher" },
    text: (
      <>
        <p>Since our local branch is now ahead of the remote's, we can push it back to the remote.</p>
        <Command>git push publisher main</Command>
        <p>Our copy editor can now fetch the latest version that includes both the corrections he made, and the changes we made.</p>
      </>
    ),
  },
];
