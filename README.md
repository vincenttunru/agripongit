# A Grip on Git

A Grip on Git is a simple, visual Git tutorial. You can find it at https://agripongit.vincenttunru.com. This repository contains the source code.

# To get set up

With `npm` installed, run `npm install`.

# To try it out

Run `npm run dev`.

# To edit just the text of the tutorial

Find the appropriate sections within [src/tutorial.tsx](https://gitlab.com/VincentTuru/agripongit/tree/main/src/tutorial.tsx) and edit them.
